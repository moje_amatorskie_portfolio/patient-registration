import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.geometry.*;
import java.io.*;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Properties;

/** klasa wyświetlająca interfejs do rejestracji pacjentów w przychodni
*@author Paweł Kubrak
*@version 1.0
*/
public class pacjenci extends Application {
  public static int strona;
  public static TextField tfImie;
  public static TextField tfNazwisko;
  public static TextField tfUlica;
  public static TextField tfNumer;
  public static TextField tfMiasto;
  public static TextField tfWojewodztwo;
  public static TextField tfPesel;
  public static TextField tfDataUrodzenia;
  public static TextField tfWyszukaj;
  public static String numerId="0";
  public static Boolean wyszukaj=false;

  public static void main(String[] args) {
    launch(args);
  }
  public void start(Stage GlowneOkno) {
    tfWyszukaj = new TextField();
    tfWyszukaj.setText("Kowalski");
    pacjenci.strona = 1;
    GlowneOkno.setTitle("Rejestracja pacjentów w przychodni medycznej");
    FlowPane GlownyPanel = new FlowPane();
    GlownyPanel.setTranslateX(0);
    GlownyPanel.setStyle("-fx-background-color:gray");
    GlownyPanel.setHgap(50);
    Scene Scena = new Scene(GlownyPanel, 1500, 600);
    FlowPane Rejestracja = new FlowPane(Orientation.HORIZONTAL);
      Rejestracja.setMaxHeight(900);
      Rejestracja.setTranslateX(80);
      Rejestracja.setTranslateY(50);
      Rejestracja.setAlignment(Pos.CENTER);
      Rejestracja.setVgap(5);
      Rejestracja.setHgap(35);
      Rejestracja.setStyle("-fx-background-color:lightgray");
      FlowPane paneNaglowekRejestracjaPacjenta = new FlowPane(Orientation.HORIZONTAL);
        paneNaglowekRejestracjaPacjenta.setStyle("-fx-background-color:darkgray;-fx-font-weight:bold;-fx-font-size:15pt;");
        paneNaglowekRejestracjaPacjenta.setAlignment(Pos.CENTER);
        Label labelNaglowekRejestracjiPacjenta = new Label("Rejestracja pacjenta");
        labelNaglowekRejestracjiPacjenta.setStyle("-fx-background-color:darkgray;");
      paneNaglowekRejestracjaPacjenta.getChildren().add(labelNaglowekRejestracjiPacjenta);
      HBox wiersz1 = new HBox(40);
        wiersz1.setTranslateY(30);
        FlowPane fpImie = new FlowPane(Orientation.VERTICAL);
          fpImie.setVgap(5);
          Label lblImie= new Label("Imie");
          pacjenci.tfImie = new TextField();
        fpImie.getChildren().addAll(lblImie,pacjenci.tfImie);
        FlowPane fpNazwisko = new FlowPane(Orientation.VERTICAL);
          fpNazwisko.setVgap(5);
          Label lblNazwisko = new Label("Nazwisko");
          pacjenci.tfNazwisko = new TextField();
        fpNazwisko.getChildren().addAll(lblNazwisko,pacjenci.tfNazwisko);
      wiersz1.getChildren().addAll(fpImie, fpNazwisko );
      HBox wiersz2 = new HBox(30);
        wiersz2.setTranslateY(-280);
        FlowPane fpUlica = new FlowPane(Orientation.VERTICAL);
          fpUlica.setVgap(5);
          Label lblUlica= new Label("Ulica");
          pacjenci.tfUlica = new TextField();
        fpUlica.getChildren().addAll(lblUlica,pacjenci.tfUlica);
        FlowPane fpNumer = new FlowPane(Orientation.VERTICAL);
          fpNumer.setVgap(5);
          Label lblNumer = new Label("Numer");
          pacjenci.tfNumer = new TextField();
        fpNumer.getChildren().addAll(lblNumer,pacjenci.tfNumer);
      wiersz2.getChildren().addAll(fpUlica, fpNumer);
      HBox wiersz3 = new HBox(30);
      wiersz3.setTranslateY(-580);
        FlowPane fpMiasto = new FlowPane(Orientation.VERTICAL);
          fpMiasto.setVgap(5);
          Label lblMiasto = new Label("Miasto");
          pacjenci.tfMiasto = new TextField();
        fpMiasto.getChildren().addAll(lblMiasto,pacjenci.tfMiasto);
        FlowPane fpWojewodztwo = new FlowPane(Orientation.VERTICAL);
          fpWojewodztwo.setVgap(5);
          Label lblWojewodztwo = new Label("Wojewodztwo");
          pacjenci.tfWojewodztwo = new TextField();
        fpWojewodztwo.getChildren().addAll(lblWojewodztwo,pacjenci.tfWojewodztwo);
      wiersz3.getChildren().addAll(fpMiasto,fpWojewodztwo);
      HBox wiersz4 = new HBox(30);
      wiersz4.setTranslateY(-870);
      FlowPane fpPesel = new FlowPane(Orientation.VERTICAL);
        fpPesel.setVgap(5);
        Label lblPesel = new Label("Pesel");
        pacjenci.tfPesel = new TextField();
      fpPesel.getChildren().addAll(lblPesel,pacjenci.tfPesel);
      FlowPane fpDataUrodzenia = new FlowPane(Orientation.VERTICAL);
        fpDataUrodzenia.setVgap(5);
        Label lblDataUrodzenia = new Label("Data Ur. (R/M/D)");
        pacjenci.tfDataUrodzenia = new TextField();
      fpDataUrodzenia.getChildren().addAll(lblDataUrodzenia,pacjenci.tfDataUrodzenia);
      wiersz4.getChildren().addAll(fpPesel,fpDataUrodzenia);
      HBox wiersz5 = new HBox(30);
      wiersz5.setTranslateY(-1140);
      Button btnLeft = new Button("<<<");
      Button btnRight = new Button(">>>");
      btnLeft.setDisable(true);
      Label lblStrona = new Label(String.valueOf(pacjenci.strona));
      GridPane siatka = new GridPane();
      Button btnWyczysc = new Button("Wyczyść");
      btnWyczysc.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent ae) {
          wyczysc();
        }
      });
      Button btnZapisz = new Button("Zapisz");
      btnZapisz.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent ae) {
          if(pacjenci.tfImie.getText().isEmpty()){pacjenci.tfImie.setStyle("-fx-background-color:red");return;}
          if(pacjenci.tfNazwisko.getText().isEmpty()){pacjenci.tfNazwisko.setStyle("-fx-background-color:red");return;}
          if(pacjenci.tfUlica.getText().isEmpty()){pacjenci.tfUlica.setStyle("-fx-background-color:red");return;}
          if(pacjenci.tfNumer.getText().isEmpty()){pacjenci.tfNumer.setStyle("-fx-background-color:red");return;}
          if(pacjenci.tfMiasto.getText().isEmpty()){pacjenci.tfMiasto.setStyle("-fx-background-color:red");return;}
          if(pacjenci.tfWojewodztwo.getText().isEmpty()){pacjenci.tfWojewodztwo.setStyle("-fx-background-color:red");return;}
          if(pacjenci.tfPesel.getText().isEmpty()){pacjenci.tfPesel.setStyle("-fx-background-color:red");return;}
          if(pacjenci.tfDataUrodzenia.getText().isEmpty()){pacjenci.tfDataUrodzenia.setStyle("-fx-background-color:red");return;}
          pacjenci.tfImie.setStyle("-fx-background-color:white");
          pacjenci.tfNazwisko.setStyle("-fx-background-color:white");
          pacjenci.tfUlica.setStyle("-fx-background-color:white");
          pacjenci.tfNumer.setStyle("-fx-background-color:white");
          pacjenci.tfMiasto.setStyle("-fx-background-color:white");
          pacjenci.tfWojewodztwo.setStyle("-fx-background-color:white");
          pacjenci.tfPesel.setStyle("-fx-background-color:white");
          pacjenci.tfDataUrodzenia.setStyle("-fx-background-color:white");
          zapisz();
          paginacja(siatka,btnLeft,btnRight,lblStrona);
        }
      });
      wiersz5.getChildren().addAll(btnWyczysc,btnZapisz);
    Rejestracja.getChildren().addAll(paneNaglowekRejestracjaPacjenta,wiersz1,wiersz2,wiersz3,wiersz4,wiersz5);
    FlowPane ListaPacjentow = new FlowPane(Orientation.VERTICAL);
    ListaPacjentow.setMaxHeight(600);
    ListaPacjentow.setTranslateY(-487);
    ListaPacjentow.setTranslateX(85);
    ListaPacjentow.setStyle("-fx-background-color:lightgray");
    ListaPacjentow.setVgap(5);
    ListaPacjentow.setPrefWrapLength(500);
    FlowPane paneNaglowekListy = new FlowPane();
    paneNaglowekListy.setStyle("-fx-background-color:darkgray;-fx-font-weight:bold;-fx-font-size:15pt;");
    paneNaglowekListy.setAlignment(Pos.CENTER);
    Label labelNaglowekListy = new Label("Lista pacjentów");
    labelNaglowekListy.setStyle("-fx-background-color:darkgray;");
    paneNaglowekListy.getChildren().add(labelNaglowekListy);
    siatka.setHgap(15);
    siatka.setVgap(7);
    btnLeft.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent ae) {
        pacjenci.strona--;
        lblStrona.setText(String.valueOf(pacjenci.strona));
        paginacja(siatka,btnLeft, btnRight, lblStrona);
        btnRight.setDisable(false);
        if(pacjenci.strona<=1) {
            btnLeft.setDisable(true);
        }
      }
    });
    btnRight.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent ae) {
        pacjenci.strona++;
        lblStrona.setText(String.valueOf(pacjenci.strona));
        paginacja(siatka,btnLeft,btnRight,lblStrona);
        btnLeft.setDisable(false);
      }
    });
    paginacja(siatka,btnLeft, btnRight,lblStrona);
    ListaPacjentow.getChildren().addAll(paneNaglowekListy,siatka);
    FlowPane Wyszukaj = new FlowPane(Orientation.HORIZONTAL);
      Wyszukaj.setMaxHeight(500);
      Wyszukaj.setTranslateX(85);
      Wyszukaj.setTranslateY(-497);
      Wyszukaj.setAlignment(Pos.CENTER);
      Wyszukaj.setVgap(30);
      Wyszukaj.setHgap(35);
      Wyszukaj.setStyle("-fx-background-color:lightgray");
    FlowPane paneNaglowekWyszukajPacjenta = new FlowPane(Orientation.HORIZONTAL);
      paneNaglowekWyszukajPacjenta.setStyle("-fx-background-color:darkgray;-fx-font-weight:bold;-fx-font-size:15pt;");
      paneNaglowekWyszukajPacjenta.setAlignment(Pos.CENTER);
      Label labelNaglowekWyszukajPacjenta = new Label("Wyszukaj pacjenta");
      labelNaglowekWyszukajPacjenta.setStyle("-fx-background-color:darkgray;");
    paneNaglowekWyszukajPacjenta.getChildren().add(labelNaglowekWyszukajPacjenta);
  VBox hbWyszukaj = new VBox(40);
    hbWyszukaj.setTranslateY(30);
    FlowPane fpWyszukajImieLubNazwisko = new FlowPane(Orientation.VERTICAL);
      fpWyszukajImieLubNazwisko.setVgap(5);
      fpWyszukajImieLubNazwisko.setTranslateY(30);
      Label lblWyszukajImieLubNazwisko= new Label("Nazwisko pacjenta");
      Button btnWyszukaj = new Button("Wyszukaj pacjenta");
      btnWyszukaj.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent ae) {
          wyszukaj=true;
          lblStrona.setText("1");
          paginacja(siatka,btnLeft,btnRight,lblStrona);
          btnLeft.setDisable(true);
          btnRight.setDisable(true);
        }
      });
      btnWyszukaj.setTranslateY(-200);
      Button btnPowrot = new Button("Powrót");
      btnPowrot.setTranslateY(-200);
      btnPowrot.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent ae) {
          strona=1;
          lblStrona.setText("1");
          wyszukaj=false;
          lblStrona.setText("1");
          paginacja(siatka,btnLeft,btnRight,lblStrona);
          btnRight.setDisable(false);
        }
      });
    fpWyszukajImieLubNazwisko.getChildren().addAll(lblWyszukajImieLubNazwisko,tfWyszukaj);
  hbWyszukaj.getChildren().addAll(fpWyszukajImieLubNazwisko,btnWyszukaj,btnPowrot );
Wyszukaj.getChildren().addAll(paneNaglowekWyszukajPacjenta,hbWyszukaj);
    GlowneOkno.setScene(Scena);
    GlownyPanel.getChildren().addAll(Rejestracja,ListaPacjentow,Wyszukaj);
    GlowneOkno.show();
  }
  /**metoda klasy pacjenci dodająca Imię, Nazwisko i Pesel pacjenta do okienka - lista pacjentów
*@return metoda nie zwraca żadnej wartości
*/
  public void dodajDoSiatki (String ID, GridPane siatka, String id, String imie, String nazwisko, String Pesel, int wiersz, Button btnLeft, Button btnRight, Label lblStrona) {
    Label lblId = new Label(id);
    Label lblImie = new Label(imie);
    Label lblNazwisko = new Label(nazwisko);
    Label lblPesel = new Label(Pesel);
    Button btnUsun = new Button("Usuń");
    Button btnEdytuj = new Button("Edytuj");
    btnEdytuj.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent ae) {
        numerId=ID;
        edytuj(ID);
      }
    });
    btnUsun.setOnAction(new EventHandler<ActionEvent>() {
      public void handle(ActionEvent ae) {
        numerId="0";
        wyczysc();
        usun(ID);
        paginacja(siatka,btnLeft,btnRight,lblStrona);
      }
    });
    siatka.add(lblId, 0, wiersz);
    siatka.add(lblImie, 1, wiersz);
    siatka.add(lblNazwisko, 2, wiersz);
    siatka.add(lblPesel, 3, wiersz);
    siatka.add(btnEdytuj,4,wiersz);
    siatka.add(btnUsun,5,wiersz);
  }
  /**metoda klasy pacjenci pobierająca dane pacjentów z bazy
*@return metoda zwraca dwuwymiarową tablicę typu String
*/
  public String[][] baza(int poczatek,int koniec, int czyWszystko){
    ResultSet resultSet;
    String tablica[][] = new String[20][10];
    try
    {
      Connection connection = DriverManager.getConnection("jdbc:mysql://77.55.215.189:3306/java","java","java");
      Statement statement = connection.createStatement();
      resultSet = null;
      if(!wyszukaj)resultSet = statement.executeQuery("select * from pacjenci order by nazwisko limit "+String.valueOf(poczatek)+","+String.valueOf(koniec));
      if(wyszukaj)resultSet = statement.executeQuery("select * from pacjenci where nazwisko like '%"+tfWyszukaj.getText()+"%' order by nazwisko;");
      int licznik = 0;
      while(resultSet.next()){
        tablica[licznik][0] = resultSet.getString(1);
        tablica[licznik][1] = resultSet.getString(2);
        tablica[licznik][2] = resultSet.getString(3);
        tablica[licznik][3] = resultSet.getString(4);
        tablica[licznik][4] = resultSet.getString(5);
        tablica[licznik][5] = resultSet.getString(6);
        tablica[licznik][6] = resultSet.getString(7);
        tablica[licznik][7] = resultSet.getString(8);
        tablica[licznik][8] = resultSet.getString(9);
        licznik++;
      }
      connection.close();
    }
    catch(Exception e)
    {
    System.err.println("Error: "+e.getMessage());
    }
    return tablica;
  }
  /**metoda klasy pacjenci tworząca paginację i dodająca do siatki typu GridPane max 10 pacjentów jednorazowo
*@return metoda nie zwraca żadnej wartości
*/
  void paginacja(GridPane siatka, Button btnLeft, Button btnRight, Label lblStrona) {
    siatka.getChildren().clear();
    siatka.add(btnLeft,2,20);
    GridPane.setHalignment(lblStrona, HPos.CENTER);
    siatka.add(lblStrona,3,20);
    siatka.add(btnRight,4,20);
    Label lblIdNaLiscie = new Label("Id");
    Label lblImieNaLiscie = new Label("Imie");
    Label lblNazwiskoNaLiscie = new Label("Nazwisko");
    Label lblPeselNaLiscie = new Label("Pesel");
    siatka.add(lblIdNaLiscie, 0, 0);
    siatka.add(lblImieNaLiscie, 1, 0);
    siatka.add(lblNazwiskoNaLiscie, 2, 0);
    siatka.add(lblPeselNaLiscie, 3, 0);
    String tablica[][] = new String[20][10];
    if(!wyszukaj)tablica = baza((pacjenci.strona-1)*10,pacjenci.strona*10,0);
    if(wyszukaj) tablica = baza(0,10,0);
    for (int i=0;i<10;i++) {
      if(tablica[i][0]!=null) {
        dodajDoSiatki(tablica[i][0],siatka,String.valueOf(((pacjenci.strona-1)*10)+i+1),tablica[i][1],tablica[i][2],tablica[i][7],i+1,btnLeft,btnRight,lblStrona);
      }
      else {
        btnRight.setDisable(true);
      }
    }
  }
  void usun(String ID){
    try
    {
      Connection connection = DriverManager.getConnection("jdbc:mysql://77.55.215.189:3306/java","java","java");
      Statement statement = connection.createStatement();
      statement.executeUpdate("delete from pacjenci where id="+ID);
      connection.close();
    }
    catch(Exception e)
    {
    System.err.println("Error:  "+e.getMessage());
    }
  }
  void edytuj(String ID) {
    ResultSet resultSet;
    try
    {
      Connection connection = DriverManager.getConnection("jdbc:mysql://77.55.215.189:3306/java","java","java");
      Statement statement = connection.createStatement();
      resultSet = statement.executeQuery("select * from pacjenci where id="+ID);
      resultSet.next();
      pacjenci.tfImie.setText(resultSet.getString(2));
      pacjenci.tfNazwisko.setText(resultSet.getString(3));
      pacjenci.tfUlica.setText(resultSet.getString(4));
      pacjenci.tfNumer.setText(resultSet.getString(5));
      pacjenci.tfMiasto.setText(resultSet.getString(6));
      pacjenci.tfWojewodztwo.setText(resultSet.getString(7));
      pacjenci.tfPesel.setText(resultSet.getString(8));
      pacjenci.tfDataUrodzenia.setText(resultSet.getString(9));
      connection.close();
    }
    catch(Exception e)
    {
    System.err.println("Error: "+e.getMessage());
    }
  }
  /**metoda klasy pacjenci dodająca nowego pacjenta do bazy lub zapisująca szczegóły edycji istniejącego pacjenta
*@return metoda nie zwraca żadnej wartości
*/
  void zapisz(){
    String imie = pacjenci.tfImie.getText();
    String nazwisko = pacjenci.tfNazwisko.getText();
    String ulica = pacjenci.tfUlica.getText();
    String numer = pacjenci.tfNumer.getText();
    String miasto = pacjenci.tfMiasto.getText();
    String wojewodztwo = pacjenci.tfWojewodztwo.getText();
    String Pesel = pacjenci.tfPesel.getText();
    String dataUrodzenia = pacjenci.tfDataUrodzenia.getText();
    ResultSet resultSet;
    try
    {
      Connection connection = DriverManager.getConnection("jdbc:mysql://77.55.215.189:3306/java","java","java");
      Statement statement = connection.createStatement();
      if(numerId!="0"){
        statement.executeUpdate("update pacjenci set imie='"+imie+"',nazwisko='"+nazwisko+"',ulica='"+ulica+"',numer='"+numer+"',miasto='"+miasto+"',wojewodztwo='"+wojewodztwo+"',pesel='"+Pesel+"',data='"+dataUrodzenia+"' where id="+numerId+";");
      }
      else {
        statement.executeUpdate("insert into pacjenci values(0,'"+imie+"','"+nazwisko+"','"+ulica+"','"+numer+"','"+miasto+"','"+wojewodztwo+"','"+Pesel+"','"+dataUrodzenia+"');");
      }
      connection.close();
    }
    catch(Exception e)
    {
    System.err.println("Error: "+e.getMessage());
    }
    wyczysc();
  }
  /**metoda klasy pacjenci czyszcząca formularz edycji pacjenta
*@return metoda nie zwraca żadnej wartości
*/
  void wyczysc(){
    numerId="0";
    pacjenci.tfImie.setText("");
    pacjenci.tfNazwisko.setText("");
    pacjenci.tfUlica.setText("");
    pacjenci.tfNumer.setText("");
    pacjenci.tfMiasto.setText("");
    pacjenci.tfWojewodztwo.setText("");
    pacjenci.tfPesel.setText("");
    pacjenci.tfDataUrodzenia.setText("");
  }
}
