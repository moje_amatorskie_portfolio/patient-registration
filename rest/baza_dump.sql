-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: java
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pacjenci`
--

DROP TABLE IF EXISTS `pacjenci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pacjenci` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imie` varchar(55) DEFAULT NULL,
  `nazwisko` varchar(55) DEFAULT NULL,
  `ulica` varchar(55) DEFAULT NULL,
  `numer` varchar(55) DEFAULT NULL,
  `miasto` varchar(55) DEFAULT NULL,
  `wojewodztwo` varchar(55) DEFAULT NULL,
  `pesel` varchar(55) DEFAULT NULL,
  `data` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pacjenci`
--

LOCK TABLES `pacjenci` WRITE;
/*!40000 ALTER TABLE `pacjenci` DISABLE KEYS */;
INSERT INTO `pacjenci` VALUES (68,'Jan','Kowalski','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(69,'Stefan','Kowalski','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(70,'Anna','Kowalska','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(71,'Anna','Nowak','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(72,'Marek','Nowak','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(73,'Jarek','Kwiatkowski','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(74,'Henryk','Różycki','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(75,'Marian','Jakubowski','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(77,'Jarek','Śliwka','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(78,'Jadwiga','Piotrowska','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(79,'Anna','Mularczyk','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(80,'Marek','Gwiazda','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(81,'Henryk','Jabłoński','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(82,'Roman','Sałata','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(83,'Jan','Kapusta','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(84,'Ryszard','Drzewiecki','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(85,'Ryszard','Trzaskolaski','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(86,'Ryszard','Aliński','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12'),(87,'Ryszard','Hubczyński','Warszawska','33','Kraków','małopolskie','22222222222','1999/12/12');
/*!40000 ALTER TABLE `pacjenci` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-25 19:16:05
